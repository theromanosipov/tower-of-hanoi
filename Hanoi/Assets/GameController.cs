﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	private Stack<GameObject> [] Towers = new Stack<GameObject>[3];
	private float timer;
	private int previousTower = 2;

	public float waitTime;
	public Transform [] TowerTransforms = new Transform[3];
	public GameObject block;
	public int number;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < Towers.Length; i++)
			Towers[i] = new Stack<GameObject>();

		if (number > 10 || number < 1) {
			//Negerai
		} else {
			for (int i = 0; i < number; i++) {
				GameObject newBlock = Instantiate( block, new Vector3(TowerTransforms[0].position.x, TowerTransforms[0].position.y + i, 0), Quaternion.identity) as GameObject;
				newBlock.transform.localScale =  new Vector3(newBlock.transform.localScale.x * Mathf.Pow(0.8f, i), newBlock.transform.localScale.y, 0);
				newBlock.GetComponent<Block>().size = number - i;
				Towers[0].Push(newBlock);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (timer < Time.time && Input.GetKey(KeyCode.Space)) {
			timer = Time.time + waitTime;
			int currentTower;
			if (number % 2 == 0)
				currentTower = (previousTower + 1) % 3;
			else
				currentTower = (previousTower + 2) % 3;
			bool isMoved = false;
			while (!isMoved) {
				int popedTower = currentTower;
				Block currentBlock = null;
				if (Towers[popedTower].Count != 0) {
					currentBlock = Towers[popedTower].Pop().GetComponent<Block>();
					while(true) {
						if (number % 2 == 0)
							currentTower = (currentTower + 1) % 3;
						else
							currentTower = (currentTower + 2) % 3;
						Block placementCandidate = null;
						if (Towers[currentTower].Count != 0)
							placementCandidate = Towers[currentTower].Peek().GetComponent<Block>();
						if (Towers[currentTower].Count == 0 || currentBlock.size < placementCandidate.size) {
							Towers[currentTower].Push(currentBlock.gameObject);
							if (currentTower != popedTower) {
								currentBlock.transform.position = new Vector3(TowerTransforms[currentTower].position.x, TowerTransforms[currentTower].position.y + Towers[currentTower].Count - 1, 0);
								isMoved = true;
								previousTower = currentTower;
							}
							break;
						}
					}
				}
				if (number % 2 == 0)
					currentTower = (currentTower + 1) % 3;
				else
					currentTower = (currentTower + 2) % 3;
			}
		}
	}

}
