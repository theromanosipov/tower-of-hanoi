﻿using UnityEngine;
using System.Collections;

class LoopIndex {

	private int value;
	private int maxValue;

	public LoopIndex (int value, int maxValue) {
		if (maxValue >= 0)
			this.maxValue = maxValue;
		else
			this.maxValue = 1;

		if (value >= maxValue)
			this.value = this.maxValue;
		else if (value < 0)
			this.value = 0;
		else
			this.value = value;
	}

	public static implicit operator int(LoopIndex i)
	{
		return i.value;
	}

	public void Increment() {
		if (value < maxValue)
			value++;
		else
			value = 0;
	}

	public void Decrement() {
		if (value < 1)
			value = maxValue;
		else
			value--;
	}
}
